db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$onSale", "fruitsOnSale": {$sum: 1}}},
	{$project: {"_id": 0}}
	]);

	$sum:1


db.fruits.aggregate([
   { $match: {"stocks": { $gte: 20 } } },
   { $count: "enoughStocks" } 
]);

db.fruits.aggregate([
     {$match: {"onSale": true}},
     {$group:{"_id": "supplier_id", avg_price: {$avg: "$price"}}}    
   ])



db.fruits.aggregate([
   {$group:
         {
           "_id": "$onSale",
           "avgAmount": { $avg: { $multiply: [ "$price", "$stocks" ] } },
           "avgQuantity": { $avg: "$price" }
         }}
])


db.fruits.aggregate([
     {$match: {"onSale": true}},
       {$group:{"_id": "supplier_id", max_price: {$max: "$price"}}
       
           {$project: {"_id": 0}}}     
])



db.fruits.aggregate([
     {$match: {"onSale": true}},
       {$group:{"_id": "supplier_id", min_price: {$min: "$price"}},
		{$project: {"_id": 0}}}
])